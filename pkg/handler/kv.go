package handler

import (
	"crypto/sha256"
	"fmt"
	"sort"
	"strings"

	vault "github.com/hashicorp/vault/api"
	corev1 "k8s.io/api/core/v1"

	"github.com/pkg/errors"

	"gitlab.com/jrr/vault-shim/pkg/config"
	"gitlab.com/jrr/vault-shim/pkg/kube"
)

// KV is the logic for checking and updating a key/value secret
func KV(kubeClient *kube.Client, vaultClient *vault.Logical, kv *config.KVSecret) error {
	vaultSecret, err := vaultClient.Read(kv.Path)

	if err != nil {
		return errors.Wrap(err, "unable to fetch Vault secrets")
	}

	if vaultSecret.Data == nil {
		return errors.New("empty secret data")
	}

	vaultData := transformData(kv, vaultSecret.Data)

	secret, err := kubeClient.GetSecret(kv.Name)

	if err != nil {
		return errors.Wrap(err, "could not check existing Kubernetes secret")
	}

	if _, managed := secret.Labels["vault-shim.jrr.io/managed"]; !managed {
		return errors.New("existing Kubernetes secret is not managed")
	}

	kubeHash := secret.Labels["vault-shim.jrr.io/hash"]

	vaultHash := hashValues(vaultData)

	if !kv.AlwaysUpdate && kubeHash == vaultHash {
		return nil
	}

	kubeSecret := buildKVSecret(kv.Name, vaultHash, vaultData)

	err = kubeClient.UpdateSecret(kubeSecret)

	if err != nil {
		return errors.Wrap(err, "unable to update Kubernetes secret")
	}

	return nil
}

func buildKVSecret(secretName, hash string, vaultData map[string]string) *corev1.Secret {
	kubeSecret := &corev1.Secret{}
	kubeSecret.Name = secretName
	kubeSecret.Type = corev1.SecretTypeOpaque

	kubeSecret.Labels = map[string]string{
		"vault-shim.jrr.io/hash": hash,
	}

	kubeSecret.Data = map[string][]byte{}

	for key, val := range vaultData {
		kubeSecret.Data[key] = []byte(val)
	}

	return kubeSecret
}

func hashValues(data map[string]string) string {
	var values []string

	for _, val := range data {
		values = append(values, val)
	}

	sort.Strings(values)

	hash := fmt.Sprintf("%x", sha256.Sum224([]byte(strings.Join(values, "!"))))

	return string(hash)
}

func transformData(kv *config.KVSecret, in map[string]interface{}) map[string]string {
	out := map[string]string{}

	for key, val := range in["data"].(map[string]interface{}) {
		builtKey := kv.EnvPrefix + key

		if kv.CapitalizeKeys {
			builtKey = strings.ToUpper(builtKey)
		}

		out[builtKey] = val.(string)
	}

	return out
}
