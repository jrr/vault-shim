package handler

import (
	"crypto/x509"
	"encoding/pem"
	"strings"
	"time"

	vault "github.com/hashicorp/vault/api"
	corev1 "k8s.io/api/core/v1"

	"github.com/pkg/errors"

	"gitlab.com/jrr/vault-shim/pkg/config"
	"gitlab.com/jrr/vault-shim/pkg/kube"
)

// TLS is the logic for checking and updating a TLS secret
func TLS(kubeClient *kube.Client, vaultClient *vault.Logical, tls *config.TLSSecret) error {
	if !tls.AlwaysIssue {
		secret, err := kubeClient.GetSecret(tls.Name)

		if err != nil {
			return errors.Wrap(err, "could not check existing Kubernetes secret")
		}

		certBytes := secret.Data[corev1.TLSCertKey]

		block, _ := pem.Decode(certBytes)

		if block == nil || block.Type != "CERTIFICATE" {
			return errors.New("existing Kubernetes secret is not in PEM format")
		}

		cert, err := x509.ParseCertificate(block.Bytes)

		if err != nil {
			return errors.Wrap(err, "unable to parse existing Kubernetes TLS secret")
		}

		if time.Until(cert.NotAfter).Hours() >= 24.0 {
			return nil
		}
	}

	vaultSecret, err := vaultClient.Write(tls.Path, tls.VaultData())

	if err != nil {
		return errors.Wrap(err, "could not issue new certificate in Vault")
	}

	err = validateTLSResponse(vaultSecret.Data)

	if err != nil {
		return errors.Wrap(err, "invalid data received from Vault")
	}

	kubeSecret := buildTLSSecret(tls.Name, vaultSecret.Data)

	err = kubeClient.UpdateSecret(kubeSecret)

	if err != nil {
		return errors.Wrap(err, "unable to update Kubernetes secret")
	}

	return nil
}

func buildTLSSecret(secretName string, vaultData map[string]interface{}) *corev1.Secret {
	var certs []string

	certs = append(certs, vaultData["certificate"].(string))

	if vaultData["ca_chain"] != nil && len(vaultData["ca_chain"].([]string)) > 0 {
		for _, cert := range vaultData["ca_chain"].([]string) {
			certs = append(certs, cert)
		}
	} else {
		certs = append(certs, vaultData["issuing_ca"].(string))
	}

	certChain := strings.Join(certs, "\n")

	kubeSecret := &corev1.Secret{}
	kubeSecret.Name = secretName
	kubeSecret.Type = corev1.SecretTypeTLS

	kubeSecret.Data = map[string][]byte{}
	kubeSecret.Data[corev1.TLSCertKey] = []byte(certChain)
	kubeSecret.Data[corev1.TLSPrivateKeyKey] = []byte(vaultData["private_key"].(string))

	return kubeSecret
}

func validateTLSResponse(secretData map[string]interface{}) error {
	if secretData == nil {
		return errors.New("empty secret data")
	}

	if secretData["issuing_ca"] == nil {
		return errors.New("missing issuing CA")
	}

	if secretData["certificate"] == nil {
		return errors.New("missing certificate")
	}

	if secretData["private_key"] == nil {
		return errors.New("missing private key")
	}

	return nil
}
