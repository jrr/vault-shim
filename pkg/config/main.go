package config

import (
	"io/ioutil"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

// Config is the overall config file.
type Config struct {
	Vault VaultConfig   `yaml:"vault"`
	KV    KVSecretList  `yaml:"kv"`
	TLS   TLSSecretList `yaml:"tls"`
}

// KVSecret is a secret mapping for key/value secrets.
type KVSecret struct {
	Name         string `yaml:"secretName"`
	Path         string `yaml:"vaultPath"`
	AlwaysUpdate bool   `yaml:"alwaysUpdate"`

	CapitalizeKeys bool   `yaml:"capitalizeKeys"`
	EnvPrefix      string `yaml:"envPrefix"`
}

// KVSecretList is an alias for []KVSecret.
type KVSecretList []KVSecret

// TLSSecret is a secret mapping for TLS certificates.
type TLSSecret struct {
	Name        string `yaml:"secretName"`
	Path        string `yaml:"vaultPath"`
	AlwaysIssue bool   `yaml:"alwaysIssue"`

	CommonName  string         `yaml:"commonName"`
	TTL         *time.Duration `yaml:"ttl"`
	DNSNames    []string       `yaml:"dnsAltNames"`
	IPAddresses []string       `yaml:"ipAddresses"`
}

// VaultData generates a request body for issuing a new cert.
func (s *TLSSecret) VaultData() map[string]interface{} {
	bodyData := map[string]interface{}{
		"common_name":        s.CommonName,
		"format":             "pem",
		"private_key_format": "pem",
	}

	if s.TTL != nil {
		bodyData["ttl"] = s.TTL
	}

	if len(s.DNSNames) > 0 {
		bodyData["alt_names"] = strings.Join(s.DNSNames, ",")
	}

	if len(s.IPAddresses) > 0 {
		bodyData["ip_sans"] = strings.Join(s.IPAddresses, ",")
	}

	return bodyData
}

// TLSSecretList is an alias for []TLSSecret.
type TLSSecretList []TLSSecret

// VaultConfig is the Vault connection and auth section..
type VaultConfig struct {
	Address  string `yaml:"address"`
	AuthPath string `yaml:"authPath"`
	CACert   string `yaml:"caCert"`
	Role     string `yaml:"role"`
}

// ReadFromFile reads the config file at path and converts it to a
// Config.
func ReadFromFile(path string) (*Config, error) {
	var config Config

	fileContents, err := ioutil.ReadFile(path)

	if err != nil {
		return nil, errors.Wrap(err, "cannot read config file")
	}

	err = yaml.Unmarshal(fileContents, &config)

	if err != nil {
		return nil, errors.Wrap(err, "cannot parse config file")
	}

	log.Info("config parsed OK")

	return &config, nil
}
