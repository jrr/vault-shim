package kube

import (
	"io/ioutil"
	"strings"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"github.com/pkg/errors"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// Client stores our clientset and running namespace.
type Client struct {
	Namespace string
	Remote    *kubernetes.Clientset
}

// Connect creates a Kubernetes client.
func Connect() (*Client, error) {
	config, err := rest.InClusterConfig()

	if err != nil {
		return nil, errors.Wrap(err, "could not read Kubernetes in-cluster config")
	}

	clientset, err := kubernetes.NewForConfig(config)

	if err != nil {
		return nil, errors.Wrap(err, "could not create Kubernetes client")
	}

	namespace, err := ioutil.ReadFile("/var/run/secrets/kubernetes.io/serviceaccount/namespace")

	if err != nil {
		return nil, errors.Wrap(err, "could not determine current namespace")
	}

	client := Client{
		Namespace: strings.TrimSpace(string(namespace)),
		Remote:    clientset,
	}

	return &client, nil
}

// GetSecret slightly abstracts fetching v1/secrets objects.
func (c *Client) GetSecret(secretName string) (*corev1.Secret, error) {
	secret, err := c.Remote.CoreV1().Secrets(c.Namespace).Get(secretName, metav1.GetOptions{})

	if err != nil {
		return nil, errors.Wrapf(err, "could not get secret %s", secretName)
	}

	return secret, nil
}

// UpdateSecret is sugar for updating v1/secrets objects.
func (c *Client) UpdateSecret(secret *corev1.Secret) error {
	if secret.Labels == nil {
		secret.Labels = map[string]string{
			"vault-shim.jrr.io/managed": "true",
		}
	} else {
		secret.Labels["vault-shim.jrr.io/managed"] = "true"
	}

	_, err := c.Remote.CoreV1().Secrets(c.Namespace).Update(secret)

	if err != nil {
		return errors.Wrap(err, "unable to update secret in Kubernetes")
	}

	return nil
}
