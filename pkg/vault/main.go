package vault

import (
	"io/ioutil"

	vault "github.com/hashicorp/vault/api"

	"github.com/pkg/errors"

	"gitlab.com/jrr/vault-shim/pkg/config"
)

func configClient(cfg *config.VaultConfig) (*vault.Client, error) {
	vaultConfig := vault.DefaultConfig()

	if cfg.CACert != "" {
		tlsConfig := &vault.TLSConfig{
			CACert: cfg.CACert,
		}

		err := vaultConfig.ConfigureTLS(tlsConfig)

		if err != nil {
			return nil, errors.Wrap(err, "cannot configure TLS for Vault connection")
		}
	}

	client, err := vault.NewClient(vaultConfig)

	if err != nil {
		return nil, errors.Wrap(err, "cannot initialize Vault client")
	}

	err = client.SetAddress(cfg.Address)

	if err != nil {
		return nil, errors.Wrap(err, "cannot set Vault address")
	}

	return client, nil
}

func verifyVault(client *vault.Client) error {
	status, err := client.Sys().SealStatus()

	if err != nil {
		return errors.Wrap(err, "cannot get Vault seal status")
	}

	if status.Sealed {
		return errors.New("Vault is sealed")
	} else if !status.Initialized {
		return errors.New("Vault is not initialized")
	}

	return nil
}

// Connect takes a VaultConfig and fetches a token from Vault.
func Connect(cfg *config.VaultConfig) (*vault.Logical, error) {
	client, err := configClient(cfg)

	if err != nil {
		return nil, errors.Wrap(err, "cannot configure Vault client")
	}

	jwt, err := ioutil.ReadFile("/var/run/secrets/kubernetes.io/serviceaccount/token")

	if err != nil {
		return nil, errors.Wrap(err, "cannot get serviceaccount token")
	}

	err = verifyVault(client)

	if err != nil {
		return nil, errors.Wrap(err, "cannot communicate with Vault")
	}

	authBody := map[string]string{
		"role": cfg.Role,
		"jwt":  string(jwt),
	}

	req := client.NewRequest("POST", "/v1/auth/"+cfg.AuthPath+"/login")

	if err = req.SetJSONBody(authBody); err != nil {
		return nil, errors.Wrap(err, "cannot set Vault auth JSON body")
	}

	resp, err := client.RawRequest(req)

	if err != nil {
		return nil, errors.Wrap(err, "cannot authenticate to Vault")
	}

	defer resp.Body.Close()

	rawSecret, err := vault.ParseSecret(resp.Body)

	if err != nil {
		return nil, errors.Wrap(err, "cannot parse Vault response")
	}

	if rawSecret.Auth == nil {
		return nil, errors.New("no authentication information received from Vault")
	}

	client.SetToken(rawSecret.Auth.ClientToken)

	return client.Logical(), nil
}
