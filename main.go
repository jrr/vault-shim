package main

import (
	"flag"
	"os"

	log "github.com/sirupsen/logrus"

	"gitlab.com/jrr/vault-shim/pkg/config"
	"gitlab.com/jrr/vault-shim/pkg/handler"
	"gitlab.com/jrr/vault-shim/pkg/kube"
	"gitlab.com/jrr/vault-shim/pkg/vault"
)

var (
	configFilePath string
)

func init() {
	flag.StringVar(&configFilePath, "config", "", "config file path (required)")
}

func main() {
	flag.Parse()

	if configFilePath == "" {
		log.Fatal("no config file path given")
	}

	appConfig, err := config.ReadFromFile(configFilePath)

	if err != nil {
		log.Fatal(err)
	}

	if len(appConfig.TLS) == 0 && len(appConfig.KV) == 0 {
		log.Info("nothing to do, quitting")
		os.Exit(0)
	}

	vaultClient, err := vault.Connect(&appConfig.Vault)

	if err != nil {
		log.Fatal(err)
	}

	log.Info("vault connected")

	kubeClient, err := kube.Connect()

	if err != nil {
		log.Fatal(err)
	}

	log.Info("kube connected")

	for _, kv := range appConfig.KV {
		kvLog := log.WithFields(log.Fields{
			"secretName": kv.Name,
		})

		kvLog.Info("handling KV secret")

		err = handler.KV(kubeClient, vaultClient, &kv)

		if err != nil {
			kvLog.Fatal(err)
		}
	}

	for _, tls := range appConfig.TLS {
		tlsLog := log.WithFields(log.Fields{
			"secretName": tls.Name,
		})

		tlsLog.Info("handling TLS secret")

		err = handler.TLS(kubeClient, vaultClient, &tls)

		if err != nil {
			tlsLog.Fatal(err)
		}
	}
}
