FROM scratch

ADD bin/vault-shim-docker /vault-shim

CMD ["/vault-shim"]
