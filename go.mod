module gitlab.com/jrr/vault-shim

go 1.12

require (
	github.com/hashicorp/vault/api v1.0.4
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.2.0
	gopkg.in/yaml.v2 v2.2.8
	k8s.io/api v0.18.2
	k8s.io/apimachinery v0.18.2
	k8s.io/client-go v0.17.0
	k8s.io/utils v0.0.0-20200414100711-2df71ebbae66 // indirect
)
