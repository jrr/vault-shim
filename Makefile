build: lint
	go build -o bin/vault-shim

build-docker: lint
	CGO_ENABLED=0 GOOS=linux go build -o bin/vault-shim-docker

lint:
	go fmt ./...
	golint -set_exit_status ./...

.PHONY: build build-docker lint
