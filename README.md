# Vault-Shim

This is an idea that came to me late of an evening. "Julie," I said aloud to
myself, "you should build that Vault/Kubernetes interconnect thing you've been
thinking about on and off for a year." And so I did.

Vault-Shim runs as an initContainer in your Kubernetes workloads and takes
care of interactions with Hashicorp Vault so your app doesn't have to about
Vault. Because we all have that one app that you can't be bothered to update.

Vault-Shim can issue certificates, which it stores as TLS-type Secrets in
Kubernetes. It can also read from a key/value engine and turn that into a
environment variable or property list. Overall operation is pretty simple,
and there are some options to slightly shift how things work.

Default behavior is not to re-issue certificates unless they expire in the 24
hours after the initContainer runs. Similarly, we store a sha224 hash of the
values from key/value secrets so that updating the Secret is not always
necessary. This may change in the future to use the Vault KV metadata.

Better documentation to come. For now all I have is "read the code". Sorry.
